<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Colores de Tarjetas') }}
        </h2>
    </x-slot>

    <div>
        <div class="w-full max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <form action="{{ route('colors.store') }}" method="POST" enctype='multipart/form-data' class="bg-white shadow-md rounded px-8 p-6 pb-8 mb-4">
                @csrf

                @if($errors->any())
                    <div role="alert">
                        <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                            Errores
                        </div>
                        <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    </div>
                @endif

                <input type="hidden" id="id" name="id" value="{{ $color->id }}" />

                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                        Nombre
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="name" name="name" type="text" placeholder="Nombre" value="{{ old("name", $color->name) }}" required>
                </div>

                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                        Orden
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="order" name="order" type="number" placeholder="Order" placeholder="0" value="{{ old("order", $color->order) }}" required>
                </div>

                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2 cursor-pointer" for="image">
                        Imagen:
                        <img alt="{{ $color->name }}" title="{{ $color->name }}" src="{{ $color->image_path }}" />
                        <p>O sube una nueva:</p>
                    </label>

                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline cursor-pointer"
                        id="image" name="image" type="file" placeholder="Imagen" value="{{ old("image") }}" required>
                </div>

                <div class="mb-4">
                    <label class=" text-gray-700 text-sm font-bold mb-2 cursor-pointer" for="is_positive">
                        ¿Es positiva?  
                    </label>
                    <input
                    class="w-5 h-5 shadow appearance-none border rounded p-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline cursor-pointer"
                        id="is_positive" name="is_positive" type="checkbox" value="1" {{ ($color->is_positive  ? 'checked' : '') }}>
                </div>
                
                <div class="text-right">
                    <a href="{{ route('colors.delete', $color->id) }}" class="mr-4 font-semibold text-xs px-4 py-2 bg-red-600 border border-transparent rounded-md text-white uppercase cursor-pointer tracking-widest hover:bg-red-700 active:bg-red-900 focus:outline-none focus:border-red-900 focus:ring focus:ring-red-300 disabled:opacity-25 transition">
                        {{ __("Eliminar") }}
                    </a>

                    <button type="submit" class="font-semibold inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
                        {{ __('Guardar') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
