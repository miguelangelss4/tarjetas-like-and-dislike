<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Colores de Tarjetas') }}
        </h2>
    </x-slot>

    <div>
        <div class="w-full max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <div class="bg-white shadow-md rounded px-8 p-6 pb-8 mb-4">
                <input type="hidden" id="id" name="id" value="{{ $color->id }}" />

                <div class="mb-4">
                    <h1 class="font-bold text-4xl text-gray-700">{{ $color->name }}</h1>
                </div>

                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2 cursor-pointer" for="image">
                        <img data-bs-toggle="tooltip" alt="{{ $color->name }}" title="{{ $color->name }}" src="{{ $color->image_path }}" />
                    </label>
                </div>

                <div class="mb-4">
                    <label class=" text-gray-700 text-sm font-bold mb-2 cursor-pointer" for="is_positive">
                        ¿Es positiva?  
                    </label>

                    <input
                    class="w-5 h-5 shadow appearance-none border rounded p-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline cursor-pointer"
                        id="is_positive" name="is_positive" onclick="return false;" type="checkbox" value="1" {{ ($color->is_positive  ? 'checked' : '') }}>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
