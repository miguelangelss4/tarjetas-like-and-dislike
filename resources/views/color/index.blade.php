<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Colores de Tarjetas') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            @can('add color')
                <a href="{{ route('colors.add') }}" 
                    class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs 
                            text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 
                            focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
                    {{ __('Añadir color') }}
                </a>
            @endcan

            @if(Session::has('success'))
                <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md mt-2" role="alert">
                    <div class="flex">
                        <div class="py-1">
                            <svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/>
                            </svg>
                        </div>

                        <div>
                            <p class="font-bold">Información</p>
                            <p class="text-sm">
                                {{ Session::get('success') }}
                            </p>
                        </div>
                    </div>
                </div>
            @endif

            @if ($colors->count() > 0)
                <div class="flex flex-wrap -mx-4">
                    @foreach ($colors as $color)
                        @can('add color')
                            @php($margen = "mb-2")
                        @else
                            @php($margen = "mb-7")
                        @endcan
                        
                        <div class="w-full md:w-1/4 px-3 {{$margen}} text-center text-gray-700">
                            @can('add color')
                                <form action="{{ route('colors.delete', $color->slug) }}" class="bg-fixed top-10 relative" style="display: flex; flex-flow: column;">
                                    <button onclick="deleteConfirm(event, this, '¡¡Se eliminarán las tarjetas asociadas también!!')" type="submit" class="rounded transition-500">
                                        {{-- relative top-10 -right-56 --}}<svg  xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="align-self: flex-end" class="float-right relative mr-4 w-6 h-6 hover:text-gray-400 transition-colors duration-400">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                        </svg>
                                    </button>
                                </form>
                            @endcan
                            
                            <a href="{{ route('colors.show', $color->slug) }}" class="rounded hover:scale-105 transition-500">
                                <div class="rounded overflow-hidden bg-white shadow-lg py-5">
                                    <img alt="{{ $color->name }}" title="{{ $color->name }}" data-bs-toggle="tooltip" class="mx-auto" src="{{ $color->image_path }}" />
                                    <p class="w-full text-center">{{ $color->name }}</span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>

                {{ $colors->links() }}
            @else
                <p>{{ __('No hay ningún color definido todavía') }}</p>                
            @endif
        </div>
    </div>
</x-app-layout>