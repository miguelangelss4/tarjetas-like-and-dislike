<div>
    @if ($cards->count() > 0)
        <div class="flex flex-wrap -mx-1 lg:-mx-4">
            @foreach ($cards as $card)
                <div class="p-4 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 transform transition-all duration-700">
                    <a href="{{ route('cards.show', $card->slug) }}"
                        class="mt-3 text-gray-700 flex rounded-lg h-full bg-white p-8 flex-col shadow-lg transform transition-all duration-500 hover:scale-105">
                        <div class="flex items-center mb-3">
                            <div
                                class="p-2 w-9 h-9 mr-3 inline-flex items-center justify-center rounded-full bg-gray-100 text-white flex-shrink-0">
                                <img src="{{ $card->color->image_path }}" class="w-5 h-5" />
                            </div>
                            <h2 class="text-gray-700 text-xl title-font font-semibold">{{ $card->title }}</h2>
                        </div>
                        <div class="flex-grow">
                            <p class="leading-relaxed text-base text-gray-700">{{ $card->excerpt() }}</p>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>

        {{ $cards->links() }}
    @else
        <p>{{ __('Este usuario no ha publicado tarjetas aún.') }}</p>
    @endif
</div>
