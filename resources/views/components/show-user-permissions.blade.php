<div>
    <form action="{{ route('users.update', $user->id) }}" method="POST">
        @csrf
        @method('PUT')

        <ul>
            @foreach ($roles as $role)
                <li>
                    <input
                        class="w-5 h-5 shadow appearance-none border rounded p-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline cursor-pointer"
                        id="roles[{{ $role->id }}]" name="roles[{{ $role->name }}]" type="checkbox" value="Roles[{{ $role->name }}]"
                        {{ ($user->hasRole($role->name) ? 'checked' : '') }}
                        wire:model="roles.{{ $role->id }}"
                        />
                    <label for="roles[{{ $role->id }}]" class="pl-1 uppercase text-gray-700 text-sm font-bold mb-2 cursor-pointer">{{ $role->name }}</label>
                </li>
            @endforeach
        </ul>

        <button type="submit" class="mt-4 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">Grabar</button>
    </form>
</div>