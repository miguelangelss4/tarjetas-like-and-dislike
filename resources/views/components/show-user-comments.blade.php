<div>
    @if ($comments->count() > 0)
        <div class="mt-5">
            @foreach ($comments as $comment)
                <!-- This is an example component -->
                <div class="relative grid grid-cols-1 gap-4 p-4 mb-8 border rounded-lg bg-white shadow-lg">
                    <div class="relative flex gap-4">
                            <div class="p-2 w-20 h-20 mr-3 inline-flex items-center justify-center rounded-full bg-gray-100 text-white flex-shrink-0"
                                data-bs-toggle="tooltip" title="{{ $comment->card->color->name }}">
                                <img 
                                    src="{{ $comment->card->color->image_path }}"
                                    title="{{ $comment->card->color->name }}" alt="{{ $comment->card->color->name }}"
                                    loading="lazy" class="w-12 h-12" />
                            </div>
                            
                        <div class="flex flex-col w-full">
                            <div class="flex flex-row justify-between">
                                <p class="relative text-xl whitespace-nowrap truncate overflow-hidden">{{ $comment->card->title }}</p>
                                @can('delete comment')
                                    <a class="text-gray-500 text-xl" href="{{ route('comments.delete', $comment->id) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="align-self: flex-end" class="mr-4 w-6 h-6 hover:text-gray-400 transition-colors duration-400">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"></path>
                                        </svg>
                                    </a>
                                @endcan
                            </div>
                            <p class="text-gray-400 text-sm">
                                Fecha comentario: {{ \Carbon\Carbon::parse($comment->created_at)->format('j F Y \\a \\l\\a\\s h:i:s A') }}
                            </p>
                            <a class="relative text-base whitespace-nowrap truncate overflow-hidden hover:underline hover:text-gray-800 transition-all transform duration-700" 
                                style="position: unset; display: contents;"
                                href="{{ route('cards.show', $comment->card->slug) }}">
                                Ver tarjeta
                            </a>
                        </div>
                    </div>
                    <p class="-mt-4 text-gray-500">{!! $comment->comment !!}</p>
                </div>
            @endforeach
        </div>

        {{ $comments->links() }}
    @else
        <p>Este usuario no ha comentado nada todavía.</p>
    @endif
</div>