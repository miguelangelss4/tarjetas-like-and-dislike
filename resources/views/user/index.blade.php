<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Usuarios') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 px-6 lg:px-8">
            @if (Session::has('success'))
                <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md"
                    role="alert">
                    <div class="flex">
                        <div class="py-1">
                            <svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20">
                                <path
                                    d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z" />
                            </svg>
                        </div>

                        <div>
                            <p class="font-bold">Información</p>
                            <p class="text-sm">
                                {{ Session::get('success') }}
                            </p>
                        </div>
                    </div>
                </div>
            @endif

            @if ($users->count() > 0)

                <table class="table-auto w-full my-4 border-collapse text-sm" style="border: 1px solid #e5e7eb">
                    <thead>
                        <tr>
                            <th
                                class="border-b dark:border-slate-600 font-medium p-4 text-slate-400 dark:text-slate-200 text-left">
                            </th>
                            <th
                                class="border-b dark:border-slate-600 font-medium p-4 text-slate-400 dark:text-slate-200 text-left">
                                Nombre</th>
                            <th
                                class="border-b dark:border-slate-600 font-medium p-4 text-slate-400 dark:text-slate-200 text-left">
                                Email</th>
                            <th
                                class="border-b dark:border-slate-600 font-medium p-4 text-slate-400 dark:text-slate-200 text-center">
                                Registro</th>
                            {{-- <th
                                class="border-b dark:border-slate-600 font-medium p-4 text-slate-400 dark:text-slate-200 text-center">
                                Suspendido hasta</th>
                            <th
                                class="border-b dark:border-slate-600 font-medium p-4 text-slate-400 dark:text-slate-200 text-center">
                                Baneado el</th> --}}
                            <th
                                class="border-b dark:border-slate-600 font-medium p-4 text-slate-400 dark:text-slate-200 text-right">
                                Admin</th>
                            <th
                                class="border-b dark:border-slate-600 font-medium p-4 text-slate-400 dark:text-slate-200 text-right">
                                Borrar</th>
                        </tr>
                    </thead>

                    <tbody class="bg-white dark:bg-slate-800">
                        @foreach ($users as $user)
                            <tr class="even:bg-gray-200">
                                <td class="text-right p-3">
                                    <img data-bs-toggle="tooltip" title="{{ $user->name }}"
                                        alt="Foto de {{ $user->name }}" src="{{ $user->profile_photo_url }}"
                                        class="w-5 h-5 rounded-full" 
                                        />
                                    </td>
                                <td class="text-left p-3">
                                    <a 
                                        class="hover:text-gray-500 hover:underline transition-all transform duration-700"
                                        href="{{ route('users.show', $user->id) }}">
                                        {{ $user->name }}
                                    </a>
                                </td>
                                <td class="text-left p-3">
                                    <a
                                        class="hover:text-gray-500 hover:underline transition-all transform duration-700"
                                        href="mailto:{{ $user->email }}">
                                        {{ $user->email }}
                                    </a>
                                </td>
                                <td class="text-center p-3">
                                    {{ \Carbon\Carbon::parse($user->created_at)->format('j-m-Y H:i') }}</td>
                                {{-- <td class="text-center p-3">
                                    {{ $user->kicked_to ? \Carbon\Carbon::parse($user->kicked_to)->format('j-M-Y H:i:s') : ' - ' }}
                                </td>
                                <td class="text-center p-3">
                                    {{ $user->banned ? \Carbon\Carbon::parse($user->banned)->format('j-M-Y H:i:s') : ' - ' }}
                                </td> --}}
                                <td class="text-right p-3">
                                    {{ $user->hasRole('admin') ? 'Sí' : 'No' }}
                                </td>
                                <td class="text-right p-3">
                                    <form action="{{ route('users.destroy', $user->id) }}">
                                        @csrf
                                        @method('DELETE')

                                        <button onclick="deleteConfirm(event, this)"
                                            class="inline-flex items-center px-4 py-2 bg-red-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-700 active:bg-red-900 focus:outline-none focus:border-red-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
                                            Borrar
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $users->links() }}
            @else
                <p>{{ __('¡No hay usuarios!') }}</p>
            @endif
        </div>
    </div>
</x-app-layout>
