<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Usuario') }}
        </h2>
    </x-slot>

    <div>
        <div class="w-full max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <div class="bg-white shadow-md rounded px-8 p-6 pb-8 mb-4">
                <div class="flex items-center mb-3">
                    <div
                        class="mr-3 inline-flex items-center justify-center rounded-full bg-gray-100 text-white flex-shrink-0">
                        <img src="{{ $user->profile_photo_url }}" data-bs-toggle="tooltip" title="{{ $user->name }}"
                            class="w-12 h-12 rounded-full" />
                    </div>
                    <h1 class="text-gray-800 text-5xl title-font font-bold">
                        {{ $user->name }}
                    </h1>
                </div>

                <div class="w-full mx-auto mt-4 rounded">
                    <!-- Tabs -->
                    <ul id="tabs" class="inline-flex w-full px-1 pt-2 ">
                        <li class="px-4 py-2 -mb-px font-semibold text-gray-800 border-b-2 border-indigo-400 rounded-t opacity-50">
                            <a id="cards-tab" href="#cards">Tarjetas</a>
                        </li>

                        <li class="px-4 py-2 font-semibold text-gray-800 rounded-t opacity-50">
                            <a id="comments-tab" href="#comments">Comentarios</a>
                        </li>

                        <li class="px-4 py-2 font-semibold text-gray-800 rounded-t opacity-50">
                            <a id="permissions-tab" href="#permissions">Permisos</a>
                        </li>
                    </ul>

                    <!-- Tab Contents -->
                    <div id="tab-contents">
                        <div id="cards" class="hidden p-4">
                            <x-show-user-cards :user-id="$user->id" :items="1" />
                        </div>
                        <div id="permissions" class="hidden p-4">
                            <x-show-user-permissions :user-id="$user->id" />
                        </div>
                        <div id="comments" class="hidden p-4">
                            <x-show-user-comments :user-id="$user->id" :items="2" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="module">
        let tabsContainer = document.querySelector("#tabs");
        let tabTogglers = tabsContainer.querySelectorAll("a");

        tabTogglers.forEach(function(toggler) {
            toggler.addEventListener("click", function(e) {
                e.preventDefault();

                let tabName = this.getAttribute("href");
                localStorage.setItem('selectedTab', tabName);

                let tabContents = document.querySelector("#tab-contents");

                for (let i = 0; i < tabContents.children.length; i++) {

                    tabTogglers[i].parentElement.classList.remove("border-indigo-400", "border-b", "-mb-px", "opacity-100");
                    tabContents.children[i].classList.remove("hidden");
                    if ("#" + tabContents.children[i].id === tabName) {
                        continue;
                    }
                    tabContents.children[i].classList.add("hidden");

                }

                e.target.parentElement.classList.add("border-indigo-400", "border-b-2", "-mb-px", "opacity-100");
            });
        });

        
        let tab = localStorage.getItem('selectedTab');
        console.log(tab);
        if(tab !== null){
            document.getElementById(tab.replaceAll('#', '') + '-tab').click();
        }
        else{
            document.getElementById("cards-tab").click();
        }
    </script>
</x-app-layout>
