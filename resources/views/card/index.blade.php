<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tarjetas') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 px-6 lg:px-8">
            @can('add card')
                <a href="{{ route('cards.add') }}"
                    class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs 
                            text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 
                            focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
                    {{ __('Añadir tarjeta') }}
                </a>
            @endcan

            @if (Session::has('success'))
                <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md mt-2"
                    role="alert">
                    <div class="flex">
                        <div class="py-1">
                            <svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20">
                                <path
                                    d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z" />
                            </svg>
                        </div>

                        <div>
                            <p class="font-bold">Información</p>
                            <p class="text-sm">
                                {{ Session::get('success') }}
                            </p>
                        </div>
                    </div>
                </div>
            @endif

            <form action="" class="mt-2 grid grid-cols-12 gap-3" autocomplete="off">
                <input name="q" placeholder="Buscar tarjetas..." autocomplete="false" class="col-span-7 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" />

                <div class="col-span-3">
                    <button id="colors-button" data-dropdown-toggle="dropdown-colors" 
                        class="text-left w-full flex-shrink-0 z-10 py-2 px-3 text-sm font-medium text-gray-700 bg-white shadow border border-gray-400 rounded hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 dark:bg-gray-700 dark:hover:bg-gray-600 dark:focus:ring-gray-700 dark:text-white dark:border-gray-800" type="button">
                        <img id="imgColorElegido" class="h-3.5 w-4.5 rounded-full mr-2 inline-block" />
                        <label id="lblColorElegido" class="inline-block">Elige color</label>
                        <svg aria-hidden="true" class="w-4 h-4 ml-1 inline-block float-right" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    </button>
                    <div id="dropdown-colors" class="z-10 hidden fixed bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700">
                        <ul class="py-2 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="colors-button">
                            @foreach ($colors as $color)
                            <li>
                                <button data-color-id="{{ $color->id }}" data-color-name="{{ $color->name }}" data-color-image="{{ $color->image_path }}" type="button" class="elige-color inline-flex w-full px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-600 dark:hover:text-white">
                                    <div class="inline-flex items-center">
                                        <img src="{{ $color->image_path }}" class="h-3.5 w-4.5 rounded-full mr-2" />
                                        {{ $color->name }}
                                    </div>
                                </button>
                            </li>
                            @endforeach
                        </ul>
                        <select id="color_id" name="color_id" title="color_id" class="hidden">
                            <option value="0">0</option>
                            @foreach ($colors as $color)
                                <option value="{{ $color->id }}">{{ $color->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <button type="submit" class="text-center col-span-2 items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">Buscar</button>
            </form>

            @if ($cards->count() > 0)
                <div class="flex flex-wrap -mx-1 lg:-mx-4">
                    @foreach ($cards as $card)
                        <div class="px-4 pb-4 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 transform transition-all duration-700">
                            @can('add card')
                                <form action="{{ route('cards.delete', $card->slug) }}" class="bg-fixed top-10 relative z-10" style="display: flex; flex-flow: column;">
                                    <button onclick="deleteConfirm(event, this, '¡¡Se eliminarán los comentarios asociados también!!')" type="submit" class="rounded transition-500">
                                        <svg  xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="align-self: flex-end" class="bg-white rounded-sm p-1 z-10 float-right relative mr-4 w-6 h-6 hover:text-gray-400 transition-colors duration-400">
                                            <path class="p-2 bg-white z-10" stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                        </svg>
                                    </button>
                                </form>
                            @endcan


                            <a href="{{ route('cards.show', $card->slug) }}" class="mt-3 text-gray-700 flex rounded-lg h-full bg-white flex-col shadow-lg transform transition-all duration-500 hover:scale-105">
                                <img class="rounded-t-lg" src="{{ $card->image_path }}" alt="{{ $card->title }}" />
                                <div class="px-8 flex items-center mb-3">
                                    <div class="p-2 w-9 h-9 mr-3 inline-flex items-center justify-center rounded-full bg-gray-100 text-white flex-shrink-0">
                                        <img src="{{ $card->color->image_path }}" class="w-5 h-5" />
                                    </div>
                                    <h2 class="text-gray-700 text-xl title-font font-semibold">{{ $card->title }}</h2>
                                </div>
                                <div class="px-8 flex-grow">
                                    <p class="leading-relaxed text-base text-gray-700">{{ $card->excerpt() }}</p>                                        
                                </div>                                        
                            </a>
                        </div>
                    @endforeach
                </div>

                {{ $cards->links() }}
            @else
                <p>{{ __('¡No hay tarjetinchis!') }}</p>
            @endif
        </div>
    </div>
    <script type="module">
        function eligeColor(color){
            $('#color_id').val(color);
            $('#dropdown-colors').toggleClass('hidden');
        }

        $('.elige-color').on('click', function(){
            eligeColor($(this).data('color-id'));

            $('#lblColorElegido').text($(this).data('color-name'));
            $('#imgColorElegido').attr('src', $(this).data('color-image'));
        });

        $('#colors-button').on('click', function(){
            $('#dropdown-colors').toggleClass('hidden');
        });
    </script>
</x-app-layout>
