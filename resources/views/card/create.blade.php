<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tarjeta') }}
        </h2>
    </x-slot>

    <div>
        <div class="w-full max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <form action="{{ route('cards.store') }}" method="POST" enctype='multipart/form-data' class="bg-white shadow-md rounded px-8 p-6 pb-8 mb-4">
                @csrf

                @if($errors->any())
                    <div role="alert">
                        <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                            Errores
                        </div>
                        <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    </div>
                @endif

                <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}" />

                <div class="flex flex-wrap mb-4">
                    <div class="w-full md:w-8/12 md:pr-4 transform transition-all transition-700">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="title">
                            Título
                        </label>
                        <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="title" name="title" type="text" placeholder="Título" value="{{ old("title") }}" required>
                    </div>

                    <div class="w-full md:w-4/12 transform transition-all transition-700">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="title">
                            Color de tarjeta
                        </label>
                        <div>
                            <button id="colors-button" data-dropdown-toggle="dropdown-colors" 
                                class="text-left w-full flex-shrink-0 z-10 py-2 px-3 text-sm font-medium text-gray-700 bg-white shadow border border-gray-400 rounded hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 dark:bg-gray-700 dark:hover:bg-gray-600 dark:focus:ring-gray-700 dark:text-white dark:border-gray-800" type="button">
                                <img id="imgColorElegido" class="h-3.5 w-4.5 rounded-full mr-2 inline-block" />
                                <label id="lblColorElegido" class="inline-block">Elige color</label>
                                <svg aria-hidden="true" class="w-4 h-4 ml-1 inline-block float-right" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            </button>
                            <div id="dropdown-colors" class="w-10/12 z-10 hidden fixed bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700">
                                <ul class="py-2 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="colors-button">
                                    @foreach ($colors as $color)
                                    <li>
                                        <button data-color-id="{{ $color->id }}" data-color-name="{{ $color->name }}" data-color-image="{{ $color->image_path }}" type="button" class="elige-color inline-flex w-full px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-600 dark:hover:text-white">
                                            <div class="inline-flex items-center">
                                                <img src="{{ $color->image_path }}" class="h-3.5 w-4.5 rounded-full mr-2" />
                                                {{ $color->name }}
                                            </div>
                                        </button>
                                    </li>
                                    @endforeach
                                </ul>
                                <select id="color_id" name="color_id" title="color_id" class="hidden">
                                    <option value="0">0</option>
                                    @foreach ($colors as $color)
                                        <option value="{{ $color->id }}">{{ $color->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="description">
                        Descripción
                    </label>

                    <textarea 
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="description" name="description" placeholder="Description" value="{{ old("description") }}"></textarea>
                </div>

                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="image">
                        Imagen
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="image" name="image" type="file" placeholder="Imagen" value="{{ old("image") }}" required>
                    
                    <span class="text-sm text-gray-500">(máximo 15MB)</span>
                </div>
                
                <div class="flex items-center justify-between">
                    <button type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">{{ __('Guardar') }}</button>
                </div>
            </form>
        </div>
    </div>
    <script type="module">
        function eligeColor(color){
            $('#color_id').val(color);
            $('#dropdown-colors').toggleClass('hidden');
        }

        $('.elige-color').on('click', function(){
            eligeColor($(this).data('color-id'));

            $('#lblColorElegido').text($(this).data('color-name'));
            $('#imgColorElegido').attr('src', $(this).data('color-image'));
        });

        $('#colors-button').on('click', function(){
            $('#dropdown-colors').toggleClass('hidden');
        });
    </script>
</x-app-layout>
