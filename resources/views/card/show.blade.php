<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tarjeta') }}
        </h2>
    </x-slot>

    <div>
        <div class="w-full max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <div class="bg-white shadow-md rounded px-8 p-6 pb-8 mb-4">
                <img class="rounded-t-lg m-auto" src="{{ $card->image_path }}" alt="{{ $card->color->name }}" title="{{ $card->color->name }}" />

                <div class="flex items-center mb-3">
                    <div
                        class="my-5 p-5 mr-3 inline-flex items-center justify-center rounded-full bg-gray-100 text-white flex-shrink-0">                        

                        <img src="{{ $card->color->image_path }}" data-bs-toggle="tooltip" title="{{ $card->color->name }}"
                            class="w-12 h-1w-12" />
                    </div>
                    <h1 class="text-gray-800 text-7xl title-font font-bold mt-5">{{ $card->title }}</h1>
                </div>

                <div class="flex items-center mb-3">
                    <div
                        class="mr-3 inline-flex items-center justify-center rounded-full flex-shrink-0">                        

                        <img src="{{ $card->user->profile_photo_url }}" data-bs-toggle="tooltip" title="{{ $card->color->name }}"
                            class="w-12 h-1w-12 rounded-full" />
                    </div>
                    <h2 class="text-gray-400 title-font font-thin">{{ $card->user->name }}, {{ $card->updated_at->format('d/m/Y H:i') }}</h2>
                </div>

                <div class="flex-grow">
                    <p class="leading-relaxed text-base text-gray-700">{{ $card->description }}</p>
                </div>

                @auth
                    <form id="frmEnviarComentario" method="POST" action="{{ route('comments.add') }}">
                        @csrf

                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}" />
                        <input type="hidden" name="card_id" value="{{ $card->id }}" />
                        <input type="hidden" name="slug" value="{{ $card->slug }}" />

                        <p class="text-sm text-gray-700 mt-4">Deja tu comentario ({{ count($card->comments) }}):</p>

                        
                        @if (Session::has('success'))
                            <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md"
                                role="alert">
                                <div class="flex">
                                    <div class="py-1">
                                        <svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path
                                                d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z" />
                                        </svg>
                                    </div>

                                    <div>
                                        <p class="font-bold">Información</p>
                                        <p class="text-sm">
                                            {{ Session::get('success') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <textarea id="comment" name="comment" title="Comment content"></textarea>

                        <button id="sendComment" type="button"
                            class="my-3 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs 
                                text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 
                                focus:ring focus:ring-gray-300 disabled:opacity-25 transition">Comentar</button>
                    </form>
                @else
                    <p class="text-sm text-gray-700 mt-3">Comentarios ({{ count($card->comments) }}):</p>
                    <p class="text-xs text-gray-600">
                        ¿Queres dejar tu comentario? 
                        <a href="{{ route('register') }}" class="transform transition-all hover:underline duration-300">¡regístrate!</a>
                    </p>
                @endauth

                <x-show-comments :card-id="$card->id" :items="2" />

            </div>
        </div>
    </div>

    @can('add comment')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.1.0/classic/ckeditor.js"></script>
        <script type="module">
            let editor;
            ClassicEditor
                .create(document.querySelector('#comment'), {
                    removePlugins: [
                        //'EasyImage',
                    ]
                })
                .then( newEditor => {
                    editor = newEditor;
                } )
                .catch(error => {
                    console.error(error);
                });

            $('#sendComment').on('click', function(e){
                e.preventDefault();
                $('#comment').val(editor.getData());
                //console.log(editor.getData());

                $('#frmEnviarComentario').submit();
            });
        </script>

        <style>
            .ck-file-dialog-button,
            button.ck.ck-button.ck-off.ck-dropdown__button[data-cke-tooltip-text='Insert table'],
            button.ck.ck-button.ck-off[data-cke-tooltip-text='Link (Ctrl+K)'] {
                display: none;
            }

            .ck-editor__editable_inline {
                min-height: 150px;
            }
        </style>
    @endcan
</x-app-layout>
