import './bootstrap';

import.meta.glob(['../img/**',]);

import Alpine from 'alpinejs';
import focus from '@alpinejs/focus';
window.Alpine = Alpine;

Alpine.plugin(focus);

Alpine.start();

import swal from 'sweetalert';
window.deleteConfirm = function (e, element, msg = '') {
    e.preventDefault();
    var form = element.form;
    swal({
        title: "¿Seguro que quieres borrarlo?",
        text: msg,
        icon: "warning",
        buttons: true,
        dangerMode: true,
        confirmButtonText: '¡Sí!',
        cancelButtonText: "¡No!",
    })
        .then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
}