<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin from command line';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        do{
            $name = $this->ask('Enter a name for the administrator', null);
        } while($name == "");

        do{
            $email = $this->ask('Enter an email for administrator', null);
        } while($email == "");

        do{
            $password = $this->ask('Enter a password for administrator', null);
        } while($password == "");
        
        \App\Models\User::factory()->create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password)
        ]);

        $user = User::where('email', env('DEFAULT_USER_EMAIL'))->first();
        $user->assignRole('admin');
        
        //DEFAULT_USER_PASSWORD = "$2y$10$nfaQC9cCY.whAGM34V0VxuMithBN.PjT6PiyDUoVWOeKjEsoGlaTm" hc&TQ7gB$iE*

        return Command::SUCCESS;
    }
}
