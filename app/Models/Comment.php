<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    
    /**
     * Get the card that owns the comment.
     */
    public function card()
    {
        return $this->belongsTo(Card::class, 'card_id', 'id');
    }

    /**
     * Get the card that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
