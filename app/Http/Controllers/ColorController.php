<?php

namespace App\Http\Controllers;

use App\Models\Color;
use App\Http\Requests\StoreColorRequest;
use App\Http\Requests\UpdateColorRequest;
use App\Models\Card;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = Color::
            orderBy("is_positive", "DESC")
            ->orderBy("order", "DESC")
            ->paginate(16);

        return view('color.index', ['colors' => $colors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreColorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreColorRequest $request)
    {
        $fields = $request->validated();

        $imageName = $fields['image']->getClientOriginalName();

        $request->image->move(public_path('uploads/img'), $imageName);
        

        $color = new Color();
        $color->name = $fields['name'];
        $color->is_positive = $fields['is_positive'];
        $color->order = $fields['order'];
        $url = convert_url_to_relative(asset("uploads/img/$imageName"));

        $color->image_path = $url;

        $color->saveOrFail();

        return redirect(route('colors.index'))
            ->with('success', 'Color creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        /** @var \App\Models\User */
        $user = auth()->user();
        
        return view($user != null && $user->hasRole('admin') ? 'color.edit' : 'color.show', ['color' => $color]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateColorRequest  $request
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateColorRequest $request, Color $color)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        try {
            DB::beginTransaction();

            $color->hasMany(Card::class)->delete();

            $color->delete();
            return redirect(route('colors.index'))
                ->with('success', 'Color eliminado correctamente');

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }
}
