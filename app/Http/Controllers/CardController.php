<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Http\Requests\StoreCardRequest;
use App\Http\Requests\UpdateCardRequest;
use App\Models\Color;
use App\Models\Comment;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Card::query()
                        ->with('color');

        if ($request->get('q')) {
            $busqueda ='%' . str_replace(' ', '%', $request->get('q')) . '%';

            $query = $query
                        ->where(function($query) use ($busqueda){
                            $query->orWhere('title', 'like', $busqueda)
                                    ->orWhere('description', 'like', $busqueda);
                        });
        }

        $color = intval($request->get('color_id'));
        if($color > 0){
            $query->where('color_id', $color);
        }

        $cards = $query->
                orderBy("created_at", "DESC")->
                paginate(8);

        $colors = 
            Color::
                orderBy("order", "DESC")->
                get();

        return view('card.index', ['cards' => $cards, 'colors' => $colors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colors = 
            Color::
                orderBy("order", "DESC")->
                get();

        return view('card.create', ['colors' => $colors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCardRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCardRequest $request)
    {
        $fields = $request->validated();

        $card = new Card();
        $card->title = $fields['title'];
        $card->description = $fields['description'];
        $card->user_id = $fields['user_id'];
        $card->color_id = $fields['color_id'];
        $card->saveOrFail();

        $imageName = $card->id . "_" . preg_replace('/\s+/', '-', $fields['image']->getClientOriginalName());
        $request->image->move(public_path('uploads/cards'), $imageName);
        $url = convert_url_to_relative(asset("uploads/cards/$imageName"));
        
        $card->image_path = $url;
        $card->updateOrFail();

        return redirect(route('cards.index'))
            ->with('success', 'Tarjeta creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        return view('card.show', ['card' => $card]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCardRequest  $request
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCardRequest $request, Card $card)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        try {
            DB::beginTransaction();

            $card->hasMany(Comment::class)->delete();

            $card->delete();
            return redirect(route('cards.index'))
                ->with('success', 'Tarjeta eliminado correctamente');

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }
}
