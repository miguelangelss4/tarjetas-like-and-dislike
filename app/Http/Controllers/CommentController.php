<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Models\Card;
use App\Models\User;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCommentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentRequest $request)
    {
        $commentValidated = $request->validated();

        $comment = new Comment();
        $comment->comment = $commentValidated['comment'];
        $comment->card_id = $commentValidated['card_id'];
        $comment->user_id = $commentValidated['user_id'];
        $comment->saveOrFail();

        return redirect(route('cards.show', $commentValidated['slug']))
            ->with('success', '¡Comentario publicado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCommentRequest  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommentRequest $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        /** @var User */
        $user = auth()->user();
        $card = Card::find($comment->card_id)->first();
        
        if($user->hasPermissionTo('delete comment')){
            $comment->deleted_by_user_id = $user->id;
            $comment->saveOrFail();

            return redirect(route('cards.show', $card->slug))
                ->with('success', '¡Comentario eliminado!');
        }

        return redirect(route('cards.show', $card->slug));
    }
}
