<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'string',
            'user_id' => 'required|numeric|min:0|not_in:0',
            'color_id' => 'required|numeric|min:0|not_in:0',
            'description' => 'string|nullable',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:15360',//15MB
        ];
    }
}
