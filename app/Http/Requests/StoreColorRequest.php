<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreColorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'is_positive' => 'nullable',
            'order' => 'required|numeric',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:20480',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'is_positive' => (bool) $this->is_positive,
        ]);
    }
}
