<?php
if (! function_exists('convert_url_to_relative')) {
    function convert_url_to_relative($string) {
        if(check_url($string))
        {
            $url = str_replace(array("http://", "https://"), "", $string);
            $url = substr($url, strpos($url, "/"));

            return $url;
        }
        else
        {
            return $string;
        }
    }
}

if (! function_exists('check_url')) {
    function check_url($string)
    {
        if (filter_var($string, FILTER_VALIDATE_URL) !== false) {
            return true;
        }
        else {
            return false;
        }
    }
}