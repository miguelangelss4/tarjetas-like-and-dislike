<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CheckBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check() && auth()->user()->kicked_to && now()->lessThan(auth()->user()->kicked_to)) {
            
            $message = 'Tu cuenta está suspendida hasta ' . \Carbon\Carbon::parse(auth()->user()->kicked_to)->format('j F Y \\a \\l\\a\\s h:i:s A') . '. Puedes contactar con un administrador para más información.';
            auth()->logout();

            return redirect()->route('login')->withMessage($message);
        }

        if (auth()->check() && auth()->user()->banned) {
            auth()->logout();
            
            $message = 'Tu cuenta está suspendida indefinidamente. Puedes contactar con un administrador para más información.';

            return redirect()->route('login')->withMessage($message);
        }

        return $next($request);
    }
}
