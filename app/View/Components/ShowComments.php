<?php

namespace App\View\Components;

use App\Models\Comment;
use Illuminate\View\Component;
use Livewire\WithPagination;

class ShowComments extends Component
{
    use WithPagination;

    public $cardId;
    public $items;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($cardId, $items = 10)
    {
        $this->cardId = $cardId;
        $this->items = $items;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $comments = Comment::
            where('deleted_by_user_id', null)->
            where('card_id', $this->cardId)->
            orderBy("created_at", "DESC")->
            paginate($this->items);
        

        return view('components.show-comments',compact('comments'));
    }
}
