<?php

namespace App\View\Components;

use App\Models\Card;
use Illuminate\View\Component;
use Livewire\WithPagination;

class ShowUserCards extends Component
{
    use WithPagination;

    public $userId;
    public $items;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($userId, $items = 10)
    {
        $this->userId = $userId;
        $this->items = $items;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $cards = Card::
                        where('user_id', $this->userId)->
                        orderBy("created_at", "DESC")->
                        paginate($this->items, ['*'], 'cardsPage');

        return view('components.users.show-user-cards', compact('cards'));
    }
}
