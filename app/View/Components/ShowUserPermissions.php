<?php

namespace App\View\Components;

use App\Models\User;
use Illuminate\View\Component;

class ShowUserPermissions extends Component
{
    public $userId;
    public array $roless = array();

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $user = User::find($this->userId);
        $roles = \Spatie\Permission\Models\Role::orderBy('name', 'ASC')->get();

        return view('components.show-user-permissions', compact('roles', 'user'));
    }
}
