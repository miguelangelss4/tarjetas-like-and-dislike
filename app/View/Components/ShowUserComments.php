<?php

namespace App\View\Components;

use App\Models\Comment;
use Illuminate\View\Component;
use Livewire\WithPagination;

class ShowUserComments extends Component
{
    public $userId;
    public $items;

    use WithPagination;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($userId, $items = 10)
    {
        $this->userId = $userId;
        $this->items = $items;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $comments = Comment::
                        where('user_id', $this->userId)->
                        orderBy("created_at", "DESC")->
                        paginate($this->items, ['*'], 'commentsPage');

        return view('components.show-user-comments', compact('comments'));
    }
}
