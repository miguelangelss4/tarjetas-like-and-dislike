La aplicación es un Laravel 9.
Para arrancar, subir a cualquier hosting con PHP 8.1 o superior, y realizar las siguentes acciones en la carpeta del proyecto:
 - ejecutar `composer install` en la terminal
 - si no existe fichero .env, duplicar y renombar .env.example y sustituir los valores de BBDD, usuario predeterminado, servicio de email etc por los correctos
 - ejecutar `php artisan key:generate`
 - ejecutar `php artisan migrate --seed` (--seed rellenará la BD con los datos definidos en los seeders: usuario por defecto, niveles de tarjetas, roles y permisos).
 - si se ha desactivado la creación de usuario por defecto, se puede crear un admin con el comando `php artisan create:admin`