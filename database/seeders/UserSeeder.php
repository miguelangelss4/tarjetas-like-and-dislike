<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creo un usuario inicial para mi
        $result = \App\Models\User::factory()->create([
            'name' => env('DEFAULT_USER_NAME'),
            'email' => env('DEFAULT_USER_EMAIL'),
            'password' => env('DEFAULT_USER_PASSWORD'), //hc&TQ7gB$iE*
        ]);

        // \App\Models\User::factory(10)->create(); //Si quisiéramos usuarios de prueba, descomentar esta línea
    }
}
