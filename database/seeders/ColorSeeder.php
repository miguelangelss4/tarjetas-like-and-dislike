<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cards = array('Platino', 'Oro', 'Plata', 'Bronce', 'Amarilla', 'Naranja', 'Roja', 'Negra');
        $order = 1700;

        foreach($cards as $card){
            $imageName = 'tarjeta-' . Str::lower($card) . '.png';
            Color::create([
                'name' => $card,
                'image_path' => '/uploads/img/' . $imageName,
                'is_positive' => $order > 1300,
                'order' => $order,
                'slug' => Str::lower($card)
            ]);
            File::copy(resource_path("img/$imageName"), public_path('uploads/img/' . $imageName));
            $order -= 100;
        }
    }
}
