<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //Creo el rol y los permisos de administrador
        $role = Role::create(['name' => 'admin']);
        $permissions = array();

        //Colores
        $permissions[] = Permission::create(['name' => 'list color']);
        $permissions[] = Permission::create(['name' => 'show color']);
        $permissions[] = Permission::create(['name' => 'add color']);
        $permissions[] = Permission::create(['name' => 'edit color']);
        $permissions[] = Permission::create(['name' => 'delete color']);

        //Tarjetas
        $permissions[] = Permission::create(['name' => 'list card']);
        $permissions[] = Permission::create(['name' => 'show card']);
        $permissions[] = Permission::create(['name' => 'add card']);
        $permissions[] = Permission::create(['name' => 'edit card']);
        $permissions[] = Permission::create(['name' => 'delete card']);

        //Comentarios
        $permissions[] = Permission::create(['name' => 'list comment']);
        $permissions[] = Permission::create(['name' => 'show comment']);
        $permissions[] = Permission::create(['name' => 'add comment']);
        $permissions[] = Permission::create(['name' => 'edit comment']);
        $permissions[] = Permission::create(['name' => 'delete comment']);

        //Usuarios
        $permissions[] = Permission::create(['name' => 'list user']);
        $permissions[] = Permission::create(['name' => 'show user']);
        $permissions[] = Permission::create(['name' => 'add user']);
        $permissions[] = Permission::create(['name' => 'edit user']);
        $permissions[] = Permission::create(['name' => 'delete user']);
        
        $role->syncPermissions($permissions);
        
        //Me elevo a admin
        $user = User::where('email', env('DEFAULT_USER_EMAIL'))->first();
        $user->assignRole('admin');
    }
}
