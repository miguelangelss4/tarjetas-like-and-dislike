<?php

use App\Http\Controllers\CardController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CardController::class, 'index'])->name('home');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
    'role:admin'
])->group(function () {
    Route::get('/colors/add', [ColorController::class, 'create'])->name('colors.add');
    Route::get('/colors/delete/{color}', [ColorController::class, 'destroy'])->name('colors.delete');
    Route::post('/colors/store', [ColorController::class, 'store'])->name('colors.store'); 

    Route::get('/cards/add', [CardController::class, 'create'])->name('cards.add');
    Route::get('/cards/delete/{card}', [CardController::class, 'destroy'])->name('cards.delete');
    Route::post('/cards/store', [CardController::class, 'store'])->name('cards.store');    
    
    Route::post('/comments/add', [CommentController::class, 'store'])->name('comments.add');
    Route::get('/comments/delete/{comment}', [CommentController::class, 'destroy'])->name('comments.delete');

    Route::resource('/users', UserController::class)->names('users');
});

Route::get('/colors/{color}', [ColorController::class, 'show'])->name('colors.show');
Route::get('/colors', [ColorController::class, 'index'])->name('colors.index');

Route::get('/cards/{card}', [CardController::class, 'show'])->name('cards.show');
Route::get('/cards', [CardController::class, 'index'])->name('cards.index');
